﻿using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace TexExporter {
  public static class Filter {
    public static byte[] ATTR_START;
    public static byte[] ATTR_END;
    public static byte[] OBJ;
    public static byte[] STREAM;
    public static byte[] LENGTH;

    public static void Init() {
      ATTR_START = StrToArr("<<");
      ATTR_END = StrToArr(">>");
      OBJ = StrToArr("obj");
      STREAM = StrToArr("stream");
      LENGTH = StrToArr("/Length");
    }

    private static byte[] StrToArr(string dat) {
      return Encoding.UTF8.GetBytes(dat);
    }
  }

  class Program {
    private static string path = "./Uebung04.pdf";

    public static void Main(string[] args) {
      Filter.Init();

      Console.WriteLine(path);

      using (Stream fs = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read)) {
        byte[] buffer = new byte[64], sBuffer;
        int ofs = 0;
        int lns = 0;
        bool streamMode = false;
        while (fs.Position < fs.Length) {
          if (!streamMode) {
            do {
              fs.Read(buffer, ofs, 1);
            } while (buffer[ofs] != '\n' && ++ofs < buffer.Length);

            streamMode = ParseLine(buffer, ofs);
            ofs = 0;
          } else {
            int streamLen = (int) _attrs["Length"], bytesToRead = streamLen, read;
            Console.WriteLine($"entering stream mode, ln: {bytesToRead}, filter: {_attrs["Filter"]}");
            using (MemoryStream ms = new MemoryStream(streamLen)) {
              sBuffer = new byte[streamLen < 512 ? streamLen : 512];
              fs.Read(sBuffer, 0, 2);
              Console.WriteLine(
                $"cut bytes: {BitConverter.ToString(sBuffer, 0, 2)} ({Encoding.UTF8.GetString(sBuffer, 0, 2)})"
              );
              while (bytesToRead > 0) {
                read = fs.Read(sBuffer, 0, bytesToRead < sBuffer.Length ? bytesToRead : buffer.Length);
                ms.Write(sBuffer, 0, read);
                bytesToRead -= read;
              }

              ms.Seek(0, SeekOrigin.Begin);
              using (MemoryStream oms = new MemoryStream(streamLen)) {
                using (DeflateStream ds = new DeflateStream(ms, CompressionMode.Decompress, true)) {
                  int offs;
                  byte[] bts;
                  while ((read = ds.Read(buffer, 0, buffer.Length)) > 0) {
                    offs = 0;
                    for (int i = 0; i < read; i++) {
                      if (buffer[i] == '\\') {
                        int number;
                        if (i + 3 < read) {
                          number = (buffer[i + 1] - 48) * 64 ///wtfking otal? who thought of this bs
                                   + (buffer[i + 2] - 48) * 8
                                   + (buffer[i + 3] - 48);
                          bts = Encoding.UTF8.GetBytes(new[] {(char) number});
                          for (int j = 0; j < bts.Length; j++) {
                            buffer[i + j] = bts[j];
                          }

                          oms.Write(buffer, offs, i + bts.Length - offs);
                          //Console.Write(Encoding.UTF8.GetString(buffer, offs, i + bts.Length - offs));
                          offs = i + 4;
                          i += 3;
                        } else {
                          number = 0;
                          int j = 0;
                          for (; j < read - i - 1; j++) {
                            number += (int) ((buffer[i + j + 1] - 48) * Math.Pow(8, 2 - j));
                          }

                          bts = new byte[3 - j];
                          ds.Read(bts, 0, bts.Length);
                          for (j = 0; j < bts.Length; j++) {
                            number += (int) ((bts[j] - 48) * Math.Pow(8, bts.Length - j - 1));
                          }

                          bts = Encoding.UTF8.GetBytes(new[] {(char) number});
                          
                          oms.Write(buffer, offs, i - offs);
                          //Console.Write(Encoding.UTF8.GetString(buffer, offs, i - offs));
                          
                          oms.Write(bts);
                         // Console.Write(Encoding.UTF8.GetString(bts));
                          offs = read;
                        }
                      }
                    }

                    if (offs < read) {
                      oms.Write(buffer, offs, read - offs);
                      //Console.Write(Encoding.UTF8.GetString(buffer, offs, read - offs));
                    }
                  }
                }

                oms.Seek(0, SeekOrigin.Begin);
                while ((read = oms.Read(sBuffer, 0, sBuffer.Length)) > 0) {
                  Console.Write(Encoding.UTF8.GetString(sBuffer, 0, read));
                }
              }
            }
          }

          if (lns > 10) break;
        }
      }
    }

    private enum State {
      Unknown,
      Obj,
      Attrs,
      AttrsDone,
    }

    private static State _state;
    private static Dictionary<string, object> _attrs;

    private static bool ParseLine(byte[] buffer, int length) {
      string txt = Encoding.UTF8.GetString(buffer, 0, length);
      Console.WriteLine(txt);

      switch (_state) {
        case State.Unknown:
          if (MatchArray(buffer, length, Filter.OBJ, true))
            _state = State.Obj;
          break;
        case State.Obj:
          if (MatchArray(buffer, length, Filter.ATTR_START)) {
            _state = State.Attrs;
            _attrs = new Dictionary<string, object>();
          }

          break;
        case State.Attrs:
          if (MatchArray(buffer, length, Filter.ATTR_END)) {
            _state = State.AttrsDone;
            break;
          }

          string[] attrData = txt.Split(' ', 2);
          attrData[0] = attrData[0].Substring(1);
          _attrs.Add(attrData[0],
            MatchArray(buffer, length, Filter.LENGTH) ? (object) int.Parse(attrData[1]) : attrData[1]
          );
          break;
        case State.AttrsDone:
          if (MatchArray(buffer, length, Filter.STREAM)) {
            _state = State.Unknown;
            return true;
          }

          break;
      }

      return false;
    }

    private static bool MatchArray(byte[] dat, int datLen, byte[] filter, bool reverse = false) {
      if (filter.Length > datLen) return false;
      if (reverse)
        for (int i = filter.Length - 1; i >= 0; i--) {
          if (dat[datLen - filter.Length + i] != filter[i]) return false;
        }
      else
        for (int i = 0; i < filter.Length; i++) {
          if (dat[i] != filter[i]) return false;
        }

      return true;
    }
  }
}