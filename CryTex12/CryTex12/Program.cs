﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.SymbolStore;
using System.IO;

namespace TexExporter {
  class Program {
    private static string path = "./Uebung04.pdf";

    public static void Main(string[] args) {
      Filter.Init();

      Debug.WriteLine(path);

      using (Stream fs = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read)) {
        using (Stream dfs = File.Open("./parse.log", FileMode.OpenOrCreate, FileAccess.Write)) {
          Parser p = new Parser(fs, new StateObject(dfs));
          p.Start();
        }
      }
      Console.WriteLine("Done");
      //Console.ReadLine();
    }
  }
}