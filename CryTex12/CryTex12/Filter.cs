﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace TexExporter {
  public class Filter {
    public static byte[] ATTR_START;
    public static byte[] ATTR_END;
    public static byte[] ENDOBJ;
    public static byte[] OBJ;
    public static byte[] STREAM;
    public static byte[] STREAM_END;

    public static byte[] LENGTH;

    //public static byte[] COMMENT;
    public static byte[] BEGIN_TEXT;
    public static byte[] END_TEXT;
    public static byte[] PDF;
    public static byte[] EOF;
    public static byte[] CHANGE_MATRIX;

    public static byte[] PATH_FILL;
    public static byte[] PATH_STROKE;
    public static byte[] PATH_RECTANGLE;
    public static byte[] PATH_DASH_PATTERN;
    public static byte[] PATH_MOVE_UNDERLINE;
    public static byte[] PATH_DRAW_UNDERLINE;
    public static byte[] PATH_LINE_WIDTH;
    public static byte[] PATH_CAP_STYLE;

    public static void Init() {
      ATTR_START = StrToArr("<<");
      ATTR_END = StrToArr(">>");
      OBJ = StrToArr(" obj");
      STREAM = StrToArr("stream");
      STREAM_END = StrToArr("endstream");
      LENGTH = StrToArr("/Length");
      //COMMENT = StrToArr("%");
      ENDOBJ = StrToArr("endobj");
      BEGIN_TEXT = StrToArr("BT");
      END_TEXT = StrToArr("ET");
      PDF = StrToArr("%PDF");
      EOF = StrToArr("%%EOF");
      CHANGE_MATRIX = StrToArr(" cm");

      PATH_FILL = StrToArr(" f");
      PATH_STROKE = StrToArr(" S");
      PATH_RECTANGLE = StrToArr(" re");
      PATH_DASH_PATTERN = StrToArr(" d");
      PATH_MOVE_UNDERLINE = StrToArr(" m");
      PATH_DRAW_UNDERLINE = StrToArr(" l");
      PATH_LINE_WIDTH = StrToArr(" w");
      PATH_CAP_STYLE = StrToArr(" J");
    }

    private readonly byte[] _matcher;
    private FilterType _type;
    private Type[] _expectetArgumets;
    private object[] _outputData;

    public StateObject.Layer.Name Name { get; }

    public Filter(StateObject.Layer.Name name, string matcher, IEnumerable<Type> expectetArguments = null, FilterType type = FilterType.LeftToRight) {
      Name = name;
      _type = type;
      _matcher = StrToArr(matcher);
      if (expectetArguments != null) {
        _expectetArgumets = expectetArguments.ToArray();
        _outputData = new object[_expectetArgumets.Length];
      }
    }

    public bool AppliesTo(byte[] data, int offset, int lastIndex, out object[] result) {
      bool matches = MatchArray(data, _matcher, offset, lastIndex + 1, _type == FilterType.RightToLeft);
      if (_expectetArgumets == null) {
        Trace.WriteLine(Name + " Filter is used without expected paremeters, if the filter doesnt need any arguments supply an empty enumerabe");
        result = null;
        return matches;
      }

      if (!matches) {
        result = null;
        return false;
      }

      if (_type == FilterType.LeftToRight) {
        offset += _matcher.Length + 1;
      } else {
        lastIndex -= _matcher.Length + 1;
      }

      if (_type == FilterType.LeftToRight) {
        for (int i = 0; i < _expectetArgumets.Length; i++) {
          _outputData[i] = HandleArgument(i, _expectetArgumets[i], data, ref offset, ref lastIndex);
        }
      } else {
        for (int i = _expectetArgumets.Length - 1; i >= 0; i--) {
          _outputData[i] = HandleArgument(i, _expectetArgumets[i], data, ref offset, ref lastIndex);
        }
      }

      result = _outputData;
      return true;
    }

    private object HandleArgument(int i, Type type, byte[] data, ref int offset, ref int lastIndex) {
      try {
        if (type == typeof(int)) {
          return FilterInt(data, ref offset, ref lastIndex);
        }

        if (type == typeof(double)) {
          return FilterDouble(data, ref offset, ref lastIndex);
        }

        if (type == typeof(string)) {
          return FilterString(data, ref offset, ref lastIndex);
        }

        if (type.IsArray) {
          if (_type == FilterType.LeftToRight) {
            if (data[offset++] != '[') {
              Trace.WriteLine($"{i + 1}th argument is declared ass array type but doesnt start with '['");
              return null;
            }
            int end = offset + 1;
            for (; end <= lastIndex; end++) {
              if (data[end] == ']') {
                break;
              }
            }
            end--;
            Type elementType = type.GetElementType();
            List<object> l = new List<object>();
            while (offset < end) {
              l.Add(HandleArgument(i, elementType, data, ref offset, ref end));
            }
            //skip over ' ' after ']'
            offset++;
            return l.ToArray();
          } else {
            if (data[lastIndex--] != ']') {
              Trace.WriteLine($"{i + 1}th argument is declared ass array type but doesnt end with ']'");
              return null;
            }
            int first = lastIndex - 1;
            for (; first >= offset; first--) {
              if (data[first] == '[') {
                break;
              }
            }
            first++;
            Type elementType = type.GetElementType();
            List<object> l = new List<object>();
            while (lastIndex > first) {
              l.Insert(0, HandleArgument(i, elementType, data, ref first, ref lastIndex));
            }
            //skip over ' ' before the '['
            lastIndex--;
            return l.ToArray();
          }
        } else {
          Trace.WriteLine($"Unsupportet type '{type}' in expectet arguments");
          _outputData[i] = null;
        }
      } catch (Exception ex) {
        Console.Error.WriteLine($"{ex.GetType()} occured while parsing argument with index {i} (processing type '{type}') in filter '{Name}'\n{ex}");
      }
      return null;
    }

    private string FilterString(byte[] data, ref int offset, ref int lastIndex) {
      if (_type == FilterType.LeftToRight) {
        int i = offset;
        for (; offset <= lastIndex; offset++) {
          if (data[offset] == ' ') break;
        }
        return Encoding.UTF8.GetString(data, i, offset++ - i);
      } else {
        int i = lastIndex;
        for (; lastIndex >= offset; lastIndex--) {
          if (data[lastIndex] == ' ') break;
        }
        return Encoding.UTF8.GetString(data, lastIndex + 1, i - lastIndex--);
      }
    }

    private int FilterInt(byte[] data, ref int offset, ref int lastIndex) {
      if (_type == FilterType.LeftToRight) {
        int sum = 0;
        for (; offset <= lastIndex; offset++) {
          if (data[offset] < '0' || data[offset] > '9') break;
          sum *= 10;
          sum += data[offset] - 48;
        }
        offset++;
        return sum;
      } else {
        int mul = 1, sum = 0;
        for (; lastIndex >= offset; lastIndex--) {
          if (data[lastIndex] < '0' || data[lastIndex] > '9') break;
          sum += (data[lastIndex] - 48) * mul;
          mul *= 10;
        }
        lastIndex--;
        return sum;
      }
    }

    private double FilterDouble(byte[] data, ref int offset, ref int lastIndex) {
      if (_type == FilterType.LeftToRight) {
        int intSum = 0, fractSum = 0, div = -1;
        for (; offset <= lastIndex; offset++) {
          if (data[offset] == '.') {
            div = 1;
            continue;
          }
          if (data[offset] < '0' || data[offset] > '9') break;
          if (div < 1) {
            intSum *= 10;
            intSum += data[offset] - 48;
          } else {
            div *= 10;
            fractSum *= 10;
            fractSum += data[offset] - 48;
          }
        }
        offset++;
        return intSum + (double)fractSum / div;
      } else {
        int div = 1;
        double sum = 0;
        for (; lastIndex >= offset; lastIndex--) {
          if (data[lastIndex] == '.') {
            sum /= div;
            div = 1;
            continue;
          }
          if (data[lastIndex] < '0' || data[lastIndex] > '9') break;
          sum += (data[lastIndex] - 48) * div;
          div *= 10;
        }
        lastIndex--;
        return sum;
      }
    }

    public enum FilterType : byte {
      LeftToRight,
      RightToLeft
    }

    private static byte[] StrToArr(string dat) {
      return Encoding.UTF8.GetBytes(dat);
    }

    public static bool MatchArray(byte[] dat, byte[] filter, int startOffset, int datLen, bool reverse = false) {
      if (filter.Length > startOffset + datLen) return false;
      if (reverse)
        for (int i = filter.Length - 1; i >= 0; i--) {
          if (dat[startOffset + datLen - filter.Length + i] != filter[i])
            return false;
        } else
        for (int i = startOffset; i < filter.Length; i++) {
          if (dat[i] != filter[i]) return false;
        }

      return true;
    }
  }
}