﻿using System.Collections;
using System.Collections.Generic;

namespace TexExporter {
  public class DualList<T1, T2> : ICollection<KeyValuePair<T1, T2>> {
    private List<KeyValuePair<T1, T2>> _data;

    public DualList() {
      _data = new List<KeyValuePair<T1, T2>>();
    }

    public IEnumerator<KeyValuePair<T1, T2>> GetEnumerator() => _data.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

    public void Add(T1 key, T2 value) => _data.Add(new KeyValuePair<T1, T2>(key, value));

    public void Add(KeyValuePair<T1, T2> item) => _data.Add(item);

    public void Clear() => _data.Clear();

    public bool Contains(KeyValuePair<T1, T2> item) => _data.Contains(item);

    public void CopyTo(KeyValuePair<T1, T2>[] array, int arrayIndex) => _data.CopyTo(array, arrayIndex);

    public bool Remove(KeyValuePair<T1, T2> item) => _data.Remove(item);

    public int Count => _data.Count;

    public bool IsReadOnly => false;

    public bool TryGetValue(T1 key, out T2 value) {
      foreach (var val in _data) {
        if (val.Key.Equals(key)) {
          value = val.Value;
          return true;
        }
      }

      value = default(T2);
      return false;
    }
    public bool TryGetValueReversed(T1 key, out T2 value) {
      for (int i = _data.Count - 1; i >= 0; i--) {
        if (_data[i].Key.Equals(key)) {
          value = _data[i].Value;
          return true;
        }
      }

      value = default(T2);
      return false;
    }
  }
}