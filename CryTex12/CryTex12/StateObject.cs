﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Numerics;
using System.Text;

namespace TexExporter {
  public class StateObject {
    private int _depth;
    public StreamWriter Writer { get; }

    public Layer RootLayer { get; }
    private Layer _currentLayer;

    public Layer CurrentLayer {
      get => _currentLayer;
    }

    public StateObject(Stream outputStream) {
      _currentLayer = RootLayer = new Layer(null, Layer.Name.root);
      Writer = new StreamWriter(outputStream, Encoding.UTF8, 1024, true);
      Writer.AutoFlush = true;
      // Writer.Write(new byte[] { 0xEF, 0xBB, 0xBF }); //unicode / utf8 marker
    }

    ~StateObject() {
      Writer?.Dispose();
    }

    public Layer SpawnChild(Layer.Name name) {
      return new Layer(_currentLayer, name);
    }

    public void AddLayer(Layer.Name name) {
      AddLayer(new Layer(_currentLayer, name));
    }

    public void AddLayer(Layer layer, BoxChar customChar = BoxChar.none) {
      if (layer.ParentLayer != _currentLayer) throw new ConstraintException("layer parent must be current layer");
      string line;
      if (_depth > 0) {
        if (customChar == BoxChar.none)
          line = (new string((char)BoxChar.ThickTopBottom, _depth - 1) +
                  (char)BoxChar.ThickTopBottomThinRight) +
                  (char)BoxChar.ThickRightBottomThinLeft;
        else
          line = new string((char)BoxChar.ThickTopBottom, _depth) + (char)customChar;
      } else {
        if (customChar == BoxChar.none)
          line = ((char)BoxChar.ThickBottomRight).ToString();
        else
          line = ((char)customChar).ToString();
      }

      line += layer.LayerName.ToString("G") + " layer";
      Writer.WriteLine(line);
      _depth++;
      _currentLayer.Children.Add(layer);
      _currentLayer = layer;
    }

    public void MoveLayerBack() {
      _depth--;
      WriteWithPrefix($"{(char)BoxChar.ThickTopRight}end {_currentLayer.LayerName:G} layer");
      _currentLayer = _currentLayer.ParentLayer;
    }

    public void WriteWithPrefix(string str, BoxChar additionalChar = BoxChar.none) {
      string line = new string((char)0x2503, _depth);
      if (additionalChar != BoxChar.none)
        line += (char)additionalChar;
      line += str;
      Writer.WriteLine(line);
    }

    public void WriteAttrWithPrefix(string attrStr, BoxChar additionalChar = BoxChar.none) {
      string line = new string((char)0x2503, _depth - 1) + (char)0x2523;
      if (additionalChar != BoxChar.none)
        line += (char)additionalChar;
      line += attrStr;
      Writer.WriteLine(line);
    }

    public enum BoxChar {
      none = 0,
      ThickTopBottom = 0x2503,
      ThickBottomRight = 0x250f,
      ThickTopRight = 0x2517,
      ThickTopBottomRight = 0x2523,

      ThickLeftThinBottom = 0x2511,
      ThickBottomThinLeft = 0x2512,
      ThickTopBottomThinRight = 0x2520,
      ThickRightBottomThinTop = 0x2522,
      ThickRightThinBottomLeft = 0x252e,
      ThickRightBottomThinLeft = 0x2532,
      ThickRightLeftThinBottom = 0x252f,
    }

    public void WriteMtx(float[] mtx) {
      string[] s = new string[mtx.Length];
      int ll = 0, rl = 0, a = 0;
      for (; a < mtx.Length; a++) {
        s[a] = mtx[a].ToString();
        if (a % 2 == 0) {
          if (s[a].Length > ll) ll = s[a].Length;
        } else {
          if (s[a].Length > rl) rl = s[a].Length;
        }
      }

      string templ = $"{{0,{ll}}} | {{1,{rl}}}";
      for (a = 0; a < s.Length; a += 2) {
        WriteWithPrefix(string.Format(templ, s[a], s[a + 1]));
      }
    }

    public void WriteMtx(Matrix3x2 mtx) {
      string[] s = {
        mtx.M11.ToString(),
        mtx.M12.ToString(),
        mtx.M21.ToString(),
        mtx.M22.ToString(),
        mtx.M31.ToString(),
        mtx.M32.ToString(),
      };
      int ll = 0, rl = 0, a = 0;
      for (; a < 6; a += 2) {
        if (s[a].Length > ll)
          ll = s[a].Length;
        if (s[a + 1].Length > rl)
          rl = s[a + 1].Length;
      }

      string templ = $"{{0,{ll}}} | {{1,{rl}}}";
      for (a = 0; a < s.Length; a += 2) {
        WriteWithPrefix(string.Format(templ, s[a], s[a + 1]));
      }
    }

    public class Layer {
      private List<Layer> _children;
      private List<string> _anonymousData;
      private DualList<string, object> _data;

      public Layer ParentLayer { get; }
      public Name LayerName { get; }

      public List<string> AnonymousData {
        get {
          if (_anonymousData == null)
            _anonymousData = new List<string>();
          return _anonymousData;
        }
      }

      public List<Layer> Children {
        get {
          if (_children == null)
            _children = new List<Layer>();
          return _children;
        }
      }

      public DualList<string, object> Data {
        get {
          if (_data == null)
            _data = new DualList<string, object>();
          return _data;
        }
      }

      public Layer(Layer parent, Name name) {
        LayerName = name;
        ParentLayer = parent;
      }

      public override string ToString() {
        return $"{LayerName:G} layer, {Children.Count} children";
      }

      public enum Name {
        root,
        custom,
        Object,
        Params,
        Stream,
        Text,
        PDF,
        GraphicState
      }
    }

    public class Path {

      public List<Instruction> Instructions { get; }

      public Path() {
        Instructions = new List<Instruction>();
      }

      public class Instruction {
        public IType Type { get; }

        public double[] Arguments { get; }

        public Instruction(IType type, double[] arguments) {
          Type = type;
          Arguments = arguments;
        }

        public enum IType {

        }
      }
    }

  }
}