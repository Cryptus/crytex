using System;
using System.Collections;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TexExporter;

namespace Tests {
  [TestClass]
  public class FilterTests {
    private byte[][] _testData;

    [TestInitialize]
    public void Setup() {
      _testData = new[] {
        Encoding.UTF8.GetBytes("test foo bar"),
        Encoding.UTF8.GetBytes("test 50 66"),
        Encoding.UTF8.GetBytes("50 66 bar"),
        Encoding.UTF8.GetBytes("test 66 3.25 furioso"),
        Encoding.UTF8.GetBytes("test 66.55 3.1415"),
        Encoding.UTF8.GetBytes("66.55 3.1415 test"),
        Encoding.UTF8.GetBytes("test [foo bar goo]"),
        Encoding.UTF8.GetBytes("test 50 [foo bar goo] 66.7 bar"),
      };
    }

    public void Internal(Type[] expArguments, string matcher, byte[] data, object[] expRes, bool expAcceptance, Filter.FilterType direction = Filter.FilterType.LeftToRight) {
      Filter f = new Filter(StateObject.Layer.Name.custom, matcher, expArguments, direction);
      bool res = f.AppliesTo(data, 0, data.Length - 1, out object[] result);
      Assert.AreEqual(expAcceptance, res, expAcceptance ? "matcher was not accepted" : "matcher should not be accepted");
      if (expAcceptance) {
        for (int i = 0; i < result.Length; i++) {
          if (result[i] == null) {
            Assert.Fail($"\nnull at index {i}\nexpected:\n{string.Join(';', expRes)}\ngot:\n{string.Join(';', result)}\n");
          }
          if (result[i].GetType().IsArray) {
            CollectionAssert.AreEqual((ICollection)expRes[i], (ICollection)result[i], $"\nexpected:\n{string.Join(';', (object[])expRes[i])}\ngot:\n{string.Join(';', (object[])result[i])}\nexpected:\n{string.Join(';', expRes)}\ngot:\n{string.Join(';', result)}\n");
          } else {
            Assert.AreEqual(expRes[i], result[i], $"\nobject at index {i} doesnt equal the expected value\nexpected:\n{string.Join(';', expRes)}\ngot:\n{string.Join(';', result)}\n");
          }
        }
      }
    }

    [TestMethod]
    public void AppliesToStringsShouldWork1() {
      Internal(new[] { typeof(string), typeof(string) }, "test", _testData[0], new object[] { "foo", "bar" }, true);
    }

    [TestMethod]
    public void AppliesToStringsReversedShouldWork1() {
      Internal(new[] { typeof(string), typeof(string) }, "bar", _testData[0], new object[] { "test", "foo" }, true, Filter.FilterType.RightToLeft);
    }


    [TestMethod]
    public void AppliesToIntsShouldWork1() {
      Internal(new[] { typeof(int), typeof(int) }, "test", _testData[1], new object[] { 50, 66 }, true);
    }

    [TestMethod]
    public void AppliesToIntsReversedShouldWork1() {
      Internal(new[] { typeof(int), typeof(int) }, "bar", _testData[2], new object[] { 50, 66 }, true, Filter.FilterType.RightToLeft);
    }


    [TestMethod]
    public void AppliesToRejectShouldWork1() {
      Internal(null, "text", _testData[0], null, false);
    }


    [TestMethod]
    public void AppliesToMixedShouldWork1() {
      Internal(new[] { typeof(int), typeof(double), typeof(string) }, "test", _testData[3], new object[] { 66, 3.25, "furioso" }, true);
    }

    [TestMethod]
    public void AppliesToMixedReversedShouldWork1() {
      Internal(new[] { typeof(string), typeof(int), typeof(double) }, "furioso", _testData[3], new object[] { "test", 66, 3.25 }, true, Filter.FilterType.RightToLeft);
    }


    [TestMethod]
    public void AppliesToDoublesShouldWork1() {
      Internal(new[] { typeof(double), typeof(double) }, "test", _testData[4], new object[] { 66.55, 3.1415 }, true);
    }

    [TestMethod]
    public void AppliesToDoublesReversedShouldWork1() {
      Internal(new[] { typeof(double), typeof(double) }, "test", _testData[5], new object[] { 66.55, 3.1415 }, true, Filter.FilterType.RightToLeft);
    }

    [TestMethod]
    public void AppliesToArraysShouldWork1() {
      Internal(new[] { typeof(string[]) }, "test", _testData[6], new object[] { new object[] { "foo", "bar", "goo" } }, true);
    }
    [TestMethod]
    public void AppliesToArraysShouldWork2() {
      Internal(new[] { typeof(int), typeof(string[]), typeof(double), typeof(string) }, "test", _testData[7], new object[] { 50, new object[] { "foo", "bar", "goo" }, 66.7, "bar" }, true);
    }

    [TestMethod]
    public void AppliesToArraysReversedShouldWork1() {
      Internal(new[] { typeof(string), typeof(int), typeof(string[]), typeof(double) }, "bar", _testData[7], new object[] { "test", 50, new object[] { "foo", "bar", "goo" }, 66.7 }, true, Filter.FilterType.RightToLeft);
    }
  }
}
