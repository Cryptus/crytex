﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;

namespace TexExporter {
  class Parser {
    private Stream _currentStream;
    private Stream _mainStream;
    private Stream _compressedStream;
    private StateObject _stateObject;

    private const int BUFFERSIZE = 1024;
    private byte[] _buffer;

    public Parser(Stream mainStream, StateObject p) {
      _currentStream = _mainStream = mainStream;
      _stateObject = p;
    }

    public void Start() {
      byte[] buffer = new byte[BUFFERSIZE];
      _buffer = new byte[BUFFERSIZE];
      int ofs = 0, read;
      do {
        read = 0;
        do {
          read += _currentStream.Read(buffer, ofs, 1);
        } while (buffer[ofs] != '\n' && ++ofs < buffer.Length);

        if (read < 1) {
          if (_currentStream != _mainStream) {
            _currentStream.Dispose();
            _currentStream = _mainStream;
            read = 1;
          }
        } else {
          //Debug.WriteLine(Encoding.UTF8.GetString(buffer, 0, ofs));
          if (ProcessLine(buffer, 0, ofs)) {
            _stateObject.WriteWithPrefix(Encoding.UTF8.GetString(buffer, 0, ofs));
          }
        }

        ofs = 0;
      } while (read > 0);
    }

    private bool ProcessLine(byte[] line, int offset, int length) {
      //any layer
      if ( /*Filter.MatchArray(line, length, Filter.COMMENT)*/line[offset] == '%') {
        //comment
        if (Filter.MatchArray(line, Filter.PDF, offset, length)) {
          _stateObject.AddLayer(StateObject.Layer.Name.PDF);
          var offs = Filter.PDF.Length + 1;
          var vers = Encoding.UTF8.GetString(line, offs, length - offs);
          _stateObject.WriteAttrWithPrefix($"Version: {vers}");
          _stateObject.CurrentLayer.Data.Add("Version", vers);
          _stateObject.CurrentLayer.Data.Add("GraphicState", new Matrix3x2(1, 0, 0, 1, 0, 0));
          return false;
        }

        if (Filter.MatchArray(line, Filter.EOF, offset, length)) {
          _stateObject.MoveLayerBack();
          return false;
        }

        _stateObject.WriteWithPrefix("Comment: ");
      } else if (Filter.MatchArray(line, Filter.OBJ, offset, length, true)) {
        _stateObject.AddLayer(StateObject.Layer.Name.Object);
        ParseNumbers(2, line, offset, length - Filter.OBJ.Length, out float[] dat);
        _stateObject.CurrentLayer.Data.Add("ID", dat[0]);
        _stateObject.CurrentLayer.Data.Add("Generation", dat[1]);
        foreach (var layerAttr in _stateObject.CurrentLayer.Data) {
          _stateObject.WriteAttrWithPrefix($"{layerAttr.Key}: {layerAttr.Value}");
        }

        return false;
      }

      //freeze mtx
      if (line[offset] == 'q') {
        _stateObject.AddLayer(StateObject.Layer.Name.GraphicState);
        if (length > 1)
          return ProcessLine(line, offset + 2, length - offset - 2);

        return false;
      }

      if (Filter.MatchArray(line, Filter.PATH_FILL, offset, length, true)) {
        _stateObject.WriteAttrWithPrefix("fill", StateObject.BoxChar.ThickBottomThinLeft);
        if (Filter.MatchArray(line, Filter.PATH_RECTANGLE, offset, length - Filter.PATH_FILL.Length, true)) {
          ParseNumbers(4, line, offset, length - Filter.PATH_FILL.Length - Filter.PATH_RECTANGLE.Length, out float[] rect);
          _stateObject.WriteWithPrefix($"rectangle: [{string.Join(' ', rect)}]", StateObject.BoxChar.ThickTopRight);
          _stateObject.CurrentLayer.Data.Add("FILL", new object[] { "rect", rect });
          return false;
        }
        return true;
      }

      if (Filter.MatchArray(line, Filter.CHANGE_MATRIX, offset, length, true)) {
        _stateObject.WriteAttrWithPrefix($"argument mxt: [{Encoding.UTF8.GetString(line, offset, length - Filter.CHANGE_MATRIX.Length)}]");
        ParseNumbers(6, line, offset, length - Filter.CHANGE_MATRIX.Length, out float[] mtx);
        // a b
        // c d
        // e f
        Matrix3x2 mmtx = new Matrix3x2(mtx[0], mtx[1], mtx[2], mtx[3], mtx[4], mtx[5]);
        //WriteMtx(mtx);
        _stateObject.WriteAttrWithPrefix("calculated mxt: ");
        StateObject.Layer layer = _stateObject.CurrentLayer;
        object oldmtx;
        while (!layer.Data.TryGetValueReversed("GraphicState", out oldmtx) && layer.ParentLayer != null) {
          layer = layer.ParentLayer;
        }

        mmtx = mmtx * (Matrix3x2)oldmtx;
        _stateObject.WriteMtx(mmtx);
        _stateObject.CurrentLayer.Data.Add("GraphicState", mmtx);
        return false;
      }

      if (Filter.MatchArray(line, Filter.ATTR_START, offset, length, true)) {
        if (length > Filter.ATTR_START.Length) {
          _stateObject.WriteAttrWithPrefix((char)StateObject.BoxChar.ThickRightLeftThinBottom +
                                           Encoding.UTF8.GetString(line, offset, length - Filter.ATTR_START.Length - 1));
          StateObject.Layer layer = _stateObject.SpawnChild(StateObject.Layer.Name.Params);
          _stateObject.CurrentLayer.Data.Add(
            Encoding.UTF8.GetString(line, offset, length - 2 - Filter.ATTR_START.Length),
            layer
          );
          _stateObject.AddLayer(layer, StateObject.BoxChar.ThickRightBottomThinTop);
        } else
          _stateObject.AddLayer(StateObject.Layer.Name.Params);

        return false;
      }

      //specified layer
      switch (_stateObject.CurrentLayer.LayerName) {
        case StateObject.Layer.Name.root:
          break;
        case StateObject.Layer.Name.Object:
          if (Filter.MatchArray(line, Filter.ENDOBJ, offset, length)) {
            _stateObject.MoveLayerBack();
            return false;
          }

          if (Filter.MatchArray(line, Filter.STREAM, offset, length)) {
            _stateObject.AddLayer(StateObject.Layer.Name.Stream);
            //flip streams
            DualList<string, object> streamParams = null;
            foreach (var layer in _stateObject.CurrentLayer.ParentLayer.Children) {
              if (layer.LayerName == StateObject.Layer.Name.Params) {
                streamParams = layer.Data;
                break;
              }
            }

            if (streamParams != null
                && streamParams.TryGetValue("/Filter", out object value)
                && value as string == "/FlateDecode" && streamParams.TryGetValue("/Length", out object lengths)) {
              _compressedStream?.Dispose();
              int streamLen = int.Parse(lengths as string), read;
              _compressedStream = new MemoryStream(streamLen);
              _stateObject.WriteAttrWithPrefix($"compressed, length: {streamLen} B");
              while (streamLen > 0) {
                read = _currentStream.Read(_buffer, 0, _buffer.Length < streamLen ? _buffer.Length : streamLen);
                _compressedStream.Write(_buffer, 0, read);
                streamLen -= read;
              }

              _compressedStream.Seek(0, SeekOrigin.Begin);
              _compressedStream.Read(_buffer, 0, 2);
              _stateObject.WriteAttrWithPrefix(
                $"Sliced bytes: {BitConverter.ToString(_buffer, 0, 2)} ({Encoding.UTF8.GetString(_buffer, 0, 2)})");
              _currentStream = new DeflateStream(_compressedStream, CompressionMode.Decompress);
            } else {
              _stateObject.WriteAttrWithPrefix("not implemented");
            }

            return false;
          }

          break;
        case StateObject.Layer.Name.Params:
          if (Filter.MatchArray(line, Filter.ATTR_END, offset, length)) {
            int i = offset;
            while (i < length && Filter.MatchArray(line, Filter.ATTR_END, offset, length)) {
              _stateObject.MoveLayerBack();
              i += 2;
            }
          } else {
            string[] split = Encoding.UTF8.GetString(line, offset, length).Split(' ', 2);
            _stateObject.CurrentLayer.Data.Add(split[0], split[1]);
            _stateObject.WriteAttrWithPrefix($"{split[0]}: {split[1]}");
          }

          return false;
        case StateObject.Layer.Name.Stream:
          if (Filter.MatchArray(line, Filter.BEGIN_TEXT, offset, length)) {
            _stateObject.AddLayer(StateObject.Layer.Name.Text);
            return false;
          }

          if (Filter.MatchArray(line, Filter.STREAM_END, offset, length)) {
            _stateObject.MoveLayerBack();
            return false;
          }

          break;
        case StateObject.Layer.Name.Text:
          if (Filter.MatchArray(line, Filter.END_TEXT, offset, length)) {
            _stateObject.MoveLayerBack();
            return false;
          }

          int nfragments = 0;
          for (int i = 0, j, k; i < length; i++) {
            if (i + 3 > length)
              continue;
            if (line[i] == ' ') {
              if (line[i + 1] == 'T') {
                if (line[i + 2] == 'f' || line[i + 2] == 'd') {
                  j = i;
                  k = 0;
                  if (line[i + 2] == 'f')
                    while (k < 2 && --j >= 0) {
                      if (line[j] == ' ') {
                        k++;
                      } else if (line[j] == '/') {
                        j--;
                        break;
                      }
                    } else
                    while (k < 2 && --j >= 0) {
                      if (line[j] == ' ') {
                        k++;
                      }
                    }

                  j++;
                  string key = Encoding.UTF8.GetString(line, i + 1, 2),
                    value = Encoding.UTF8.GetString(line, j, i - j);
                  _stateObject.CurrentLayer.Data.Add(
                    key,
                    value
                  );
                  // _stateObject.WriteAttrWithPrefix($"data: {key}: {value}");
                  nfragments++;
                }
              }
            } else if (line[i] == ']') {
              if (line[i + 1] == 'T' && line[i + 2] == 'J') {
                j = i;
                k = 0;
                while (k < 1 && --j >= 0) {
                  if (line[j] == ' ') {
                    k++;
                  }
                }

                j++;
                string key = Encoding.UTF8.GetString(line, i + 1, 2),
                  value = Encoding.UTF8.GetString(line, j, i - j + 1);
                _stateObject.CurrentLayer.Data.Add(
                  key,
                  value
                );
                // _stateObject.WriteAttrWithPrefix($"data {key}: {value}");
                nfragments++;
              }
            }
          }

          if (nfragments < 1)
            _stateObject.CurrentLayer.AnonymousData.Add(Encoding.UTF8.GetString(line, offset, length));
          else {
            _stateObject.WriteAttrWithPrefix($"parsed data fragments: {nfragments}");
          }
          break;
        case StateObject.Layer.Name.GraphicState:
          if (line[0] == 'Q' && length == 1) {
            //restore mtx
            _stateObject.MoveLayerBack();
            return false;
          }

          break;
      }

      return true;
    }

    private bool ParseNumbers(int expectedAmount, byte[] line, int offset, int length, out float[] nbrs) {
      nbrs = new float[expectedAmount];
      for (int i = offset + length - 1, j = expectedAmount - 1, k = 1; i >= offset; i--) {
        if (line[i] == ' ') {
          k = 1;
          j--;
          if (j < offset) return true;
        } else if (line[i] == '.') {
          nbrs[j] /= k;
          k = 1;
        } else {
          nbrs[j] += (line[i] - 48) * k;
          k *= 10;
        }
      }

      return false;
    }
  }
}